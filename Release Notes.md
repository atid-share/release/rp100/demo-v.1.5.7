## RELEASE HISTORY: Unitech Reader Demo  
#### _Release date: 2021-11-25_

#### {+Demo version : 1.5.7.2021112501+}

#### Tested FW version: ut-5.1.1.13.3

#### What's New:
- Inventory Rail Tag Type Feature added
  * by default 6C tag inventory is set.
     [ Settings - > Inventory Tag Type -> (6C/6B/Rail)]
      <details>
     <summary markdown="span" >Screenshots</summary>
      <div align="left">
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/b059b3af8f3a5593c0e900ea97c3efb31bf10199/tag_type_settings_01.png"  alt="Setting screen" title="Settings"</img>
        <b>==></b>
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/b059b3af8f3a5593c0e900ea97c3efb31bf10199/tag_type_settings_02.png" alt="List screen" title="Setting Lists"></img>
        <b>==></b>
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/b059b3af8f3a5593c0e900ea97c3efb31bf10199/tag_type_settings_03.png" alt="dialog " title="Option dialog"></img>
       </div>
      </details>
